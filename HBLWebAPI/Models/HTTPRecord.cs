﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HBLWebAPI.Models
{
    public class HTTPRecord
    {
        public String ClientIP { get; set; }
        public String ServerIP { get; set; }
        public String HTTPMethod { get; set; }
        public String BrowserType { get; set; }
        public String ReqURL { get; set; }
    }
}