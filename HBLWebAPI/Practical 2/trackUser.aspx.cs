﻿using HBLWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HBLWebAPI.Practical_2
{
    public partial class trackUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string browser = null;
            // Getting IP of visitor
            string ipClient = Request.ServerVariables["REMOTE_ADDR"];
            // Getting ip of server
            string ipServer = Request.ServerVariables["LOCAL_ADDR"];

            string RequestMethod = Request.ServerVariables["REQUEST_METHOD"];
            // Getting the page which called the script
            string requUrl = Request.ServerVariables["URL"];
            string refererPage = Request.ServerVariables["HTTP_REFERER"];
            // Getting Browser name of visitor
            if ((Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE")))
            {
                browser = "Internet Explorer";
            }
            else if ((Request.ServerVariables["HTTP_USER_AGENT"].Contains("FireFox")))
            {
                browser = "FireFox";
            }
            else if ((Request.ServerVariables["HTTP_USER_AGENT"].Contains("Opera")))
            {
                browser = "Opera";
            }
            else if ((Request.ServerVariables["HTTP_USER_AGENT"].Contains("Chrome")))
            {
                browser = "Chrome";
            }

            /*
            Response.Write(Request.UserAgent + "<br/>");
            Response.Write("Your HTTP Method: " + RequestMethod);
            Response.Write("<br/>");
            Response.Write("Your Browser: " + browser);
            Response.Write("<br/>");
            Response.Write("Client IP Address: " + ipClient);
            Response.Write("<br/>");
            Response.Write("Server IP Address: " + ipServer);
            Response.Write("<br/>");
            Response.Write("Your requested URL: " + requUrl);
            Response.Write("<br/>");
            Response.Write("Your Referer URL: " + refererPage);
            */
            //save to object model
            HTTPRecord httpTrack = new HTTPRecord();
            httpTrack.HTTPMethod = RequestMethod;
            httpTrack.ClientIP = ipClient;
            httpTrack.ServerIP = ipServer;
            httpTrack.ReqURL = requUrl;
            //serialize to JSON
            string jsonString = JsonConvert.SerializeObject(httpTrack, Formatting.Indented);
            Response.Write(jsonString);
            // To write to a file, create a StreamWriter object.
            string jsonFile = HttpContext.Current.Server.MapPath("~/App_Data/myJSON.json");
            StreamWriter myWriter = new StreamWriter(jsonFile);
            myWriter.WriteAsync(jsonString);
            myWriter.Close();
        }
    }
}